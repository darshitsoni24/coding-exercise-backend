package com.example.crud.example.service;

import com.example.crud.example.entity3.Menu;
import com.example.crud.example.repository.MenuRepository;

import org.jvnet.hk2.annotations.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MenuService {

    @Autowired
    private MenuRepository repository;

    public Menu saveMenu(Menu menu) {
        return repository.save(menu);
    }

    public List<Menu> saveMenus(List<Menu> menus) {
        return repository.saveAll(menus);
    }


    public List<Menu> getMenus() {
        return repository.findAll();
    }

    public Menu getMenuById(int id) {
        return repository.findById(id).orElse(null);
    }

    public String deleteMenu(int id) {
        repository.deleteById(id);
        return "Menu removed !!" + id;
    }

    public Menu updateMenu(Menu menu) {
        Menu existingMenu = repository.findById(menu.getId()).orElse(null);
        existingMenu.setName(menu.getName());
        return existingMenu;
    }
}
