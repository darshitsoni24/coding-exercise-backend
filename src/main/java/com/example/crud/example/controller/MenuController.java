package com.example.crud.example.controller;

import com.example.crud.example.entity3.Menu;
import com.example.crud.example.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.PreparedStatement;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
public class MenuController {

    @Autowired
    private MenuService service;

    @PostMapping("/addMenu")
    public Menu addMenu(@RequestBody Menu menu) {
        return service.saveMenu(menu);
    }

    @PostMapping("/addMenus")
    public List<Menu> addMenus(@RequestBody List<Menu> menus) {
        return service.saveMenus(menus);
    }

    @GetMapping("/Menus")
    public List<Menu> findAllMenus() {
        return service.getMenus();
    }

    @GetMapping("/AllMenus")
    public List<Menu> findAllMenusByTree() {
        return service.getMenus().stream()
                .filter(menu -> Objects.isNull(menu.getParent()))
                .collect(Collectors.toList());
    }


    @GetMapping("/Menu/{id}")
    public Menu findMenuById(@PathVariable int id) {
        return service.getMenuById(id);
    }

    @PutMapping("/update")
    public Menu updateMenu(@RequestBody Menu menu) {
        return service.updateMenu(menu);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteMenu(@PathVariable int id) {
        return service.deleteMenu(id);
    }
}
