package com.example.crud.example.repository;

import com.example.crud.example.entity3.Menu;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MenuRepository extends JpaRepository<Menu, Integer> {
}
